# Les mines d'Eoz

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/mines-eoz.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/mines-eoz.git
```
