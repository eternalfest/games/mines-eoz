package eoz;

import patchman.Ref;
import patchman.IPatch;
import etwin.Obfu;
import etwin.ds.FrozenArray;
import custom_clips.CustomClips;

@:build(patchman.Build.di())
class Eoz {

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new(customClips: CustomClips, patches: Array<IPatch>) {
        patches = patches.concat([
            Ref.auto(hf.entity.bad.walker.Pomme.onShoot).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Pomme): Void {
                var pepin = hf.entity.shoot.Pepin.attach(self.game, self.x, self.y);
                pepin.shootSpeed = 50;
                if (self.dir < 0) {
                    pepin.moveLeft(pepin.shootSpeed);
                } else {
                    pepin.moveRight(pepin.shootSpeed);
                }
            }),

            Ref.auto(hf.entity.bad.walker.Ananas.startAttack).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Ananas): Void {
                var v2 = true;
                var v3 = self.game.getPlayerList();

                for (v4 in 0...v3.length) {
                    if (!v3[v4].fl_knock) {
                        v2 = false;
                    }
                }
                if (v2) {
                    return;
                }
                self.halt();
                self.playAnim(hf.Data.ANIM_BAD_THINK);
                self.forceLoop(true);
                self.setNext(0, -25, hf.Data.SECOND * 0.9, hf.Data.ACTION_MOVE);
                self.fl_attack = true;
                var v5 = self.game.depthMan.attach('curse', hf.Data.DP_FX);
                v5.gotoAndStop('' + hf.Data.CURSE_TAUNT);
                self.stick(v5, 0, -hf.Data.CASE_HEIGHT * 2.5);
            }),

            Ref.auto(hf.entity.bad.walker.Poire.onShoot).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Poire): Void {
                var bombe = customClips.substitute("hammer_bomb_repel", "hammer_bomb_poire_frozen", function() {
                    return hf.entity.bomb.player.RepelBomb.attach(self.game, self.x, self.y);
                });
                if (self.dir < 0) {
                    bombe.moveToAng(-135, 10);
                } else {
                    bombe.moveToAng(-45, 10);
                }
                self.setNext(null, null, self.shootDuration, hf.Data.ACTION_FALLBACK);
            }),

            Ref.auto(hf.entity.bad.walker.Fraise.onShoot).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Fraise): Void {
                Reflect.callMethod(self, untyped hf.entity.bad.Shooter.prototype.attack, []);
                self.fl_ball = false;
                if ((self.ballTarget.types & hf.Data.BAD) > 0) {
                    var v3: hf.entity.bad.walker.Fraise = cast self.ballTarget;
                    v3.setNext(null, null, hf.Data.BALL_TIMEOUT + 5, hf.Data.ACTION_WALK);
                    v3.halt();
                    v3.playAnim(hf.Data.ANIM_BAD_THINK);
                }
                var balle = hf.entity.shoot.Ball.attach(self.game, self.x, self.y);
                balle.setLifeTimer(null);
                balle.shootSpeed = 10;
                balle.moveToTarget(self.ballTarget, balle.shootSpeed);
                balle.targetCatcher = cast self.ballTarget;
            }),

            Ref.auto(hf.entity.shoot.Ball.hit).replace(function(hf: hf.Hf, self: hf.entity.shoot.Ball, e: hf.Entity): Void {
                if ((e.types & hf.Data.BOMB) > 0) {
                    var v3: hf.entity.Bomb = cast e;
                    if (!v3.fl_explode)
                    {
                        self.game.fxMan.attachFx(self.x, self.y - hf.Data.CASE_HEIGHT / 2, 'hammer_fx_pop');
                        v3.destroy();
                        self.game.fxMan.inGameParticles(hf.Data.PARTICLE_CLASSIC_BOMB, self.x, self.y, 6);
                        self.destroy();
                        return;
                    }
                }
                if ((e.types & hf.Data.PLAYER) > 0) {
                    var v4: hf.entity.Player = cast e;
                    v4.killHit(self.dx);
                }
                if ((e.types & hf.Data.CATCHER) > 0 && self.targetCatcher == e) {
                    var v5: hf.entity.bad.walker.Fraise = cast e;
                    v5.catchBall(self);
                }
            }),

            Ref.auto(hf.entity.bad.ww.Crawler.attack).replace(function(hf: hf.Hf, self: hf.entity.bad.ww.Crawler): Void {
                var v2 = hf.entity.bomb.bad.PoireBomb.attach(self.game, self.x, self.y);
                v2.moveTo(self.x, self.y);
                v2.dx = -self.cp.x * hf.entity.bad.ww.Crawler.SHOOT_SPEED;
                v2.dy = -self.cp.y * hf.entity.bad.ww.Crawler.SHOOT_SPEED;
                v2.scale(70);
                var v3 = hf.Std.random(3) + 2;
                if (self.cp.x != 0) {
                    self.game.fxMan.inGameParticlesDir(hf.Data.PARTICLE_BLOB, self.x, self.y, v3, -self.cp.x);
                } else {
                    self.game.fxMan.inGameParticles(hf.Data.PARTICLE_BLOB, self.x, self.y, v3);
                }
                self.game.fxMan.attachExplosion(self.x, self.y, 20);
                self.sub._xscale = 150 + Math.abs(self.cp.x) * 150;
                self.sub._yscale = 150 + Math.abs(self.cp.y) * 150;
                self.colorAlpha = hf.entity.bad.ww.Crawler.COLOR_ALPHA;
                self.setColorHex(Math.round(self.colorAlpha), hf.entity.bad.ww.Crawler.COLOR);
                self.attackCD = hf.entity.bad.ww.Crawler.COOLDOWN;
                self.playAnim(hf.Data.ANIM_BAD_SHOOT_END);
            }),

            Ref.auto(hf.entity.bad.walker.Litchi.weaken).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Litchi): Void {
                var child = hf.entity.bad.walker.LitchiWeak.attach(self.game, self.x, self.y - hf.Data.CASE_HEIGHT);
                child.angerMore();
                child.updateSpeed();
                child.halt();
                child.dir = self.dir;
                self.game.fxMan.inGameParticles(hf.Data.PARTICLE_LITCHI, self.x + hf.Std.random(20) * (hf.Std.random(2) * 2 - 1), self.y - hf.Std.random(20), hf.Std.random(3) + 5);
                child.playAnim(hf.Data.ANIM_BAD_SHOOT_START);
            }),

            Ref.auto(hf.entity.bad.walker.Litchi.dropReward).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Litchi): Void {

            }),

            Ref.auto(hf.entity.bad.walker.LitchiWeak.dropReward).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.LitchiWeak): Void {

            }),

            Ref.auto(hf.entity.bad.walker.Framboise.onShoot).replace(function(hf: hf.Hf, self: hf.entity.bad.walker.Framboise): Void {
                var v6 = true;
                var v2 = 0;
                var v3 = 0;
                var v4 = 0;
                while (v2 < hf.entity.bad.walker.Framboise.MAX_TRIES && v6) {
                    v6 = false;
                    v3 = hf.Std.random(hf.Data.LEVEL_WIDTH);
                    v4 = hf.Std.random(hf.Data.LEVEL_HEIGHT);
                    var v5 = self.distanceCase(v3, v4);

                    if (v5 <= 7) {
                        v6 = true;
                    }
                    if (!self.game.world.checkFlag({x: v3, y: v4}, hf.Data.IA_TILE_TOP)) {
                        v6 = true;
                    }
                    if (self.game.world.checkFlag({x: v3, y: v4}, hf.Data.IA_SMALL_SPOT)) {
                        v6 = true;
                    }
                    if ((self.game.getListAt(hf.Data.SPEAR, v3, v4)).length > 0) {
                        v6 = true;
                    }
                    ++v2;
                }
                if (v2 >= hf.entity.bad.walker.Framboise.MAX_TRIES) {
                    return;
                }
                self.tx = hf.Entity.x_ctr(v3);
                self.ty = hf.Entity.y_ctr(v4);
                self.arrived = 0;
                self.phaseOut();

                for (v8 in 0...hf.entity.bad.walker.Framboise.FRAGS) {
                    var grain = hf.entity.shoot.FramBall.attach(self.game, self.aroundX(self.x), self.aroundY(self.y));
                    grain.shootSpeed = 1 + hf.Std.random(0);
                    grain.setLifeTimer(hf.Data.SECOND * 40);
                    grain.setOwner(self);
                    grain.gotoAndStop('' + (v8 + 1));
                }
            }),

            Ref.auto(hf.mode.GameMode.attachBad).replace(function(hf: hf.Hf, self: hf.mode.GameMode, id: Int, x: Float, y: Float): hf.entity.Bad {
                var bad: hf.entity.Bad = null;

                if (id == hf.Data.BAD_POMME) {
                    var pomme = hf.entity.bad.walker.Pomme.attach(self, x, y);
                    pomme.initShooter(40, 12);
                    bad = pomme;
                }
                else if (id == hf.Data.BAD_CERISE) {
                    var cerise = hf.entity.bad.walker.Cerise.attach(self, x, y);
                    cerise.animFactor = 2.15;
                    cerise.setFall(20);
                    bad = cerise;

                }
                else if (id == hf.Data.BAD_BANANE) {
                    var banane = hf.entity.bad.walker.Banane.attach(self, x, y);
                    banane.setJumpUp(5000);
                    banane.setJumpDown(5000);
                    bad = banane;
                }
                else if (id == hf.Data.BAD_ANANAS) {
                    var ananas = hf.entity.bad.walker.Ananas.attach(self, x, y);
                    bad = ananas;
                }
                else if (id == hf.Data.BAD_ABRICOT) {
                    var abricot = hf.entity.bad.walker.Abricot.attach(self, x, y, true);
                    abricot._visible = false;
                    bad = abricot;
                }
                else if (id == hf.Data.BAD_ABRICOT2) {
                    var abricot2 = hf.entity.bad.walker.Abricot.attach(self, x, y, false);
                    abricot2._visible = false;
                    bad = abricot2;
                }
                else if (id == hf.Data.BAD_POIRE) {
                    var poire = hf.entity.bad.walker.Poire.attach(self, x, y);
                    bad = poire;
                }
                else if (id == hf.Data.BAD_BOMBE) {
                    var bombino = hf.entity.bad.walker.Bombe.attach(self, x, y);
                    bombino.blinkColor = 10752202;
                    bombino.blinkColorAlpha = 500;
                    bad = bombino;
                }
                else if (id == hf.Data.BAD_ORANGE) {
                    var orange = customClips.substitute("hammer_bad_orange", "hammer_bad_baleine", function() {
                        return hf.entity.bad.walker.Orange.attach(self, x, y);
                    });
                    orange.setJumpUp(5);
                    orange.setFall(20);
                    bad = orange;
                }
                else if (id == hf.Data.BAD_FRAISE) {
                    var fraise = hf.entity.bad.walker.Fraise.attach(self, x, y);
                    bad = fraise;
                }
                else if (id == hf.Data.BAD_CITRON) {
                    var citron = hf.entity.bad.walker.Citron.attach(self, x, y);
                    citron.initShooter(5, 2);
                    bad = citron;
                }
                else if (id == hf.Data.BAD_BALEINE) {
                    var pasteque = customClips.substitute("hammer_bad_baleine", "hammer_bad_orange", function() {
                        return hf.entity.bad.flyer.Baleine.attach(self, x, y);
                    });
                    bad = pasteque;
                }
                else if (id == hf.Data.BAD_SPEAR) {
                    var pic = hf.entity.bad.Spear.attach(self, x, y);
                    bad = pic;
                }
                else if (id == hf.Data.BAD_CRAWLER) {
                    var blob = hf.entity.bad.ww.Crawler.attach(self, x, y);
                    bad = blob;
                }
                else if (id == hf.Data.BAD_TZONGRE) {
                    var tzongre = hf.entity.bad.flyer.Tzongre.attach(self, x, y);
                    bad = tzongre;
                }
                else if (id == hf.Data.BAD_SAW) {
                    var scie = hf.entity.bad.ww.Saw.attach(self, x, y);
                    bad = scie;
                }
                else if (id == hf.Data.BAD_LITCHI) {
                    var litchi = hf.entity.bad.walker.Litchi.attach(self, x, y);
                    bad = litchi;
                }
                else if (id == hf.Data.BAD_KIWI) {
                    var kiwi = hf.entity.bad.walker.Kiwi.attach(self, x, y);
                    kiwi.angerFactor = 10;
                    bad = kiwi;
                }
                else if (id == hf.Data.BAD_LITCHI_WEAK) {
                    var litchinu = hf.entity.bad.walker.LitchiWeak.attach(self, x, y);
                    bad = litchinu;
                }
                else if (id == hf.Data.BAD_FRAMBOISE) {
                    var framboise = hf.entity.bad.walker.Framboise.attach(self, x, y);
                    bad = framboise;
                }
                else {
                    hf.GameManager.fatal('(attachBad) unknown bad ' + id);
                }

                if (bad.isType(hf.Data.BAD_CLEAR)) {
                    ++self.badCount;
                }
                return bad;

            }),
        ]);
        this.patches = FrozenArray.from(patches);
    }
}
